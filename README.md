# Nodos de Kubernetes

En un clúster de Kubernetes, encontrará dos categorías distintas de nodos:

## Nodos maestros
Estos nodos desempeñan un papel fundamental en la gestión de las llamadas de API de control para varios componentes dentro del clúster de Kubernetes. Esto incluye la supervisión de pods, controladores de replicación, servicios, nodos y más.

## Nodos de trabajo
Los nodos de trabajo son responsables de proporcionar entornos de ejecución para los contenedores. Vale la pena señalar que un grupo de pods de contenedores puede extenderse a través de varios nodos de trabajo, lo que garantiza una asignación y administración óptimas de los recursos.

# Prerrequisitos

Antes de comenzar la instalación, asegúrese de que su entorno cumpla con los siguientes requisitos previos:

- Un sistema **Ubuntu 24.04 LTS**.
- Acceso privilegiado al sistema (**usuario root** o **sudo**).
- Conexión a Internet activa.
- Mínimo **2 GB de RAM** o más.
- Mínimo **2 núcleos de CPU** (o **2 vCPU**).
- **20 GB de espacio libre en disco** en `/var` (o más).

## Paso 1: Actualizar y mejorar Ubuntu (todos los nodos)

Comience por asegurarse de que su sistema esté actualizado. Abra una terminal y ejecute los siguientes comandos:

```bash
sudo apt update && sudo apt upgrade -y
```

## Paso 2: Deshabilitar el intercambio (todos los nodos)

Para mejorar el rendimiento de Kubernetes, deshabilite el intercambio y configure los parámetros esenciales del kernel. Ejecute los siguientes comandos en todos los nodos para deshabilitar todos los intercambios:

```bash
sudo swapoff -a 
sudo sed -i '/ swap / s/^\(.*\)$/#\1/g' /etc/fstab
```

## Paso 3: Agregar parámetros del kernel (todos los nodos)
Cargue los módulos del kernel necesarios en todos los nodos:

```bash
sudo tee /etc/modules-load.d/containerd.conf << EOF 
overlay 
br_netfilter 
EOF
sudo modprobe overlay 
sudo modprobe br_netfilter
```

Configure los parámetros críticos del kernel para Kubernetes utilizando lo siguiente:

```bash
sudo tee /etc/sysctl.d/kubernetes.conf << EOF 
net.bridge.bridge-nf-call-ip6tables = 1 
net.bridge.bridge-nf-call-iptables = 1 
net.ipv4.ip_forward = 1 
EOF
```

Luego, vuelva a cargar los cambios:

```bash
sudo sysctl --system
```

## Paso 4: Instalar Containerd Runtime (todos los nodos)
Estamos utilizando el entorno de ejecución de Containerd. Instale Containerd y sus dependencias con los siguientes comandos:

```bash
sudo apt install -y curl gnupg2 software-properties-common apt-transport-https ca-certificates
```

Habilitar el repositorio Docker:

```bash
sudo curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmour -o /etc/apt/trusted.gpg.d/docker.gpg 
sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"
```

Actualice la lista de paquetes e instale Containerd:

```bash
sudo apt update 
sudo apt install -y containerd.io
```
Configurar Containerd para comenzar a usar systemd como cgroup:

```bash
containerd config default | sudo tee /etc/containerd/config.toml >/dev/null 2>&1 
sudo sed -i 's/SystemdCgroup = false/SystemdCgroup = true/g' /etc/containerd/config.toml
```

Reinicie y habilite el servicio Containerd:

```bash
sudo systemctl restart containerd 
sudo systemctl enable containerd
```

## Paso 5: Agregar el repositorio Apt para Kubernetes (todos los nodos)
Los paquetes de Kubernetes no están disponibles en los repositorios predeterminados de Ubuntu 22.04. Agregue los repositorios de Kubernetes con los siguientes comandos:

```bash
echo "deb [signed-by=/etc/apt/keyrings/kubernetes-apt-keyring.gpg] https://pkgs.k8s.io/core:/stable:/v1.30/deb/ /" | sudo tee /etc/apt/sources.list.d/kubernetes.list 
curl -fsSL https://pkgs.k8s.io/core:/stable:/v1.30/deb/Release.key | sudo gpg --dearmor -o /etc/apt/keyrings/kubernetes-apt-keyring.gpg
```

## Paso 6: Instalar Kubectl, Kubeadm y Kubelet (todos los nodos)

Después de agregar los repositorios, instale los componentes esenciales de Kubernetes, incluidos `kubectl`, `kubelet` y `kubeadm`, en todos los nodos con los siguientes comandos:

```bash
sudo apt update 
sudo apt install -y kubelet kubeadm kubectl 
sudo apt-mark hold kubelet kubeadm kubectl
```

## Paso 7: Inicializar el clúster de Kubernetes con Kubeadm (nodo maestro)
Con todos los requisitos previos establecidos, inicialice el clúster de Kubernetes en el nodo maestro utilizando el siguiente comando:

```bash
sudo kubeadm init
```

```bash
root@gebo-mza-master:~# sudo kubeadm init
[init] Using Kubernetes version: v1.30.3
[preflight] Running pre-flight checks
[preflight] Pulling images required for setting up a Kubernetes cluster
[preflight] This might take a minute or two, depending on the speed of your internet connection
[preflight] You can also perform this action in beforehand using 'kubeadm config images pull'
W0810 16:57:59.292992   15792 checks.go:844] detected that the sandbox image "registry.k8s.io/pause:3.8" of the container runtime is inconsistent with that used by kubeadm.It is recommended to use "registry.k8s.io/pause:3.9" as the CRI sandbox image.
[certs] Using certificateDir folder "/etc/kubernetes/pki"
[certs] Generating "ca" certificate and key
[certs] Generating "apiserver" certificate and key
[certs] apiserver serving cert is signed for DNS names [kubernetes kubernetes.default kubernetes.default.svc kubernetes.default.svc.cluster.local master] and IPs [10.96.0.1 192.168.48.236]
[certs] Generating "apiserver-kubelet-client" certificate and key
[certs] Generating "front-proxy-ca" certificate and key
[certs] Generating "front-proxy-client" certificate and key
[certs] Generating "etcd/ca" certificate and key
[certs] Generating "etcd/server" certificate and key
[certs] etcd/server serving cert is signed for DNS names [localhost master] and IPs [192.168.48.236 127.0.0.1 ::1]
[certs] Generating "etcd/peer" certificate and key
[certs] etcd/peer serving cert is signed for DNS names [localhost master] and IPs [192.168.48.236 127.0.0.1 ::1]
[certs] Generating "etcd/healthcheck-client" certificate and key
[certs] Generating "apiserver-etcd-client" certificate and key
[certs] Generating "sa" key and public key
[kubeconfig] Using kubeconfig folder "/etc/kubernetes"
[kubeconfig] Writing "admin.conf" kubeconfig file
[kubeconfig] Writing "super-admin.conf" kubeconfig file
[kubeconfig] Writing "kubelet.conf" kubeconfig file
[kubeconfig] Writing "controller-manager.conf" kubeconfig file
[kubeconfig] Writing "scheduler.conf" kubeconfig file
[etcd] Creating static Pod manifest for local etcd in "/etc/kubernetes/manifests"
[control-plane] Using manifest folder "/etc/kubernetes/manifests"
[control-plane] Creating static Pod manifest for "kube-apiserver"
[control-plane] Creating static Pod manifest for "kube-controller-manager"
[control-plane] Creating static Pod manifest for "kube-scheduler"
[kubelet-start] Writing kubelet environment file with flags to file "/var/lib/kubelet/kubeadm-flags.env"
[kubelet-start] Writing kubelet configuration to file "/var/lib/kubelet/config.yaml"
[kubelet-start] Starting the kubelet
[wait-control-plane] Waiting for the kubelet to boot up the control plane as static Pods from directory "/etc/kubernetes/manifests"
[kubelet-check] Waiting for a healthy kubelet. This can take up to 4m0s
[kubelet-check] The kubelet is healthy after 1.004099735s
[api-check] Waiting for a healthy API server. This can take up to 4m0s
[api-check] The API server is healthy after 6.502017982s
[upload-config] Storing the configuration used in ConfigMap "kubeadm-config" in the "kube-system" Namespace
[kubelet] Creating a ConfigMap "kubelet-config" in namespace kube-system with the configuration for the kubelets in the cluster
[upload-certs] Skipping phase. Please see --upload-certs
[mark-control-plane] Marking the node master as control-plane by adding the labels: [node-role.kubernetes.io/control-plane node.kubernetes.io/exclude-from-external-load-balancers]
[mark-control-plane] Marking the node master as control-plane by adding the taints [node-role.kubernetes.io/control-plane:NoSchedule]
[bootstrap-token] Using token: 52mvip.mc0zj0qxd0jn5h0v
[bootstrap-token] Configuring bootstrap tokens, cluster-info ConfigMap, RBAC Roles
[bootstrap-token] Configured RBAC rules to allow Node Bootstrap tokens to get nodes
[bootstrap-token] Configured RBAC rules to allow Node Bootstrap tokens to post CSRs in order for nodes to get long term certificate credentials
[bootstrap-token] Configured RBAC rules to allow the csrapprover controller automatically approve CSRs from a Node Bootstrap Token
[bootstrap-token] Configured RBAC rules to allow certificate rotation for all node client certificates in the cluster
[bootstrap-token] Creating the "cluster-info" ConfigMap in the "kube-public" namespace
[kubelet-finalize] Updating "/etc/kubernetes/kubelet.conf" to point to a rotatable kubelet client certificate and key
[addons] Applied essential addon: CoreDNS
[addons] Applied essential addon: kube-proxy

Your Kubernetes control-plane has initialized successfully!

To start using your cluster, you need to run the following as a regular user:

  mkdir -p $HOME/.kube
  sudo cp -i /etc/kubernetes/admin.conf $HOME/.kube/config
  sudo chown $(id -u):$(id -g) $HOME/.kube/config

Alternatively, if you are the root user, you can run:

  export KUBECONFIG=/etc/kubernetes/admin.conf

You should now deploy a pod network to the cluster.
Run "kubectl apply -f [podnetwork].yaml" with one of the options listed at:
  https://kubernetes.io/docs/concepts/cluster-administration/addons/

Then you can join any number of worker nodes by running the following on each as root:

sudo kubeadm join 192.168.48.236:6443 --token 52mvip.mc0zj0qxd0jn5h0v \
        --discovery-token-ca-cert-hash sha256:f831d1efd0a008c7649085ba396550180d9f5a1b00e626f765d16466583ab024 

root@master:~#  mkdir -p $HOME/.kube
  sudo cp -i /etc/kubernetes/admin.conf $HOME/.kube/config
  sudo chown $(id -u):$(id -g) $HOME/.kube/config

Una vez completada la inicialización, tome nota del comando kubeadm join para referencia futura.

Ejecute los siguientes comandos en el nodo maestro:

mkdir -p $HOME/.kube
sudo cp -i /etc/kubernetes/admin.conf $HOME/.kube/config
sudo chown $(id -u):$(id -g) $HOME/.kube/config
```

A continuación, utilice kubectllos comandos para comprobar el estado del clúster y del nodo:

```bash
kubectl get nodes
```

```bash
diegoc@diegoc-ThinkPad-E15-Gen-2:~/Documents/workspace-itc/gebo-mza/Manifests$ kubectl get nodes
NAME                 STATUS   ROLES                       AGE   VERSION
gebo-mza-master      Ready    control-plane               18d   v1.30.6
gebo-mza-worker-01   Ready    control-plane,etcd,worker   18d   v1.30.6
gebo-mza-worker-02   Ready    control-plane,etcd,worker   18d   v1.30.6
gebo-mza-worker-03   Ready    control-plane,etcd,worker   18d   v1.30.6
```

## Paso 8: Agregar nodos de trabajo al clúster (nodos de trabajo)
En cada nodo de trabajo, utilice el comando kubeadm join que anotó anteriormente:

```bash
sudo kubeadm join 192.168.48.236:6443 --token 52mvip.mc0zj0qxd0jn5h0v --discovery-token-ca-cert-hash sha256:f831d1efd0a008c7649085ba396550180d9f5a1b00e626f765d16466583ab024
```

## Paso 9: Instalar el complemento de red de Kubernetes (nodo maestro)
Para habilitar la comunicación entre pods en el clúster, necesita un complemento de red. Instale el complemento de red de Calico con el siguiente comando desde el nodo maestro:

```bash
kubectl apply -f https://raw.githubusercontent.com/projectcalico/calico/v3.25.0/manifests/calico.yaml
```

## Paso 10: Verificar el clúster y realizar pruebas (nodo maestro)
Por último, queremos verificar si nuestro clúster se creó correctamente.

```bash
kubectl get pods -n kube-system
kubectl get nodes
```

```bash
diegoc@diegoc-ThinkPad-E15-Gen-2:~/Documents/workspace-itc/gebo-mza/Manifests$ kubectl get pods -n kube-system
NAME                                              READY   STATUS    RESTARTS      AGE
calico-kube-controllers-564985c589-dwtr4          1/1     Running   1 (12d ago)   12d
calico-node-8p9rj                                 1/1     Running   0             8d
calico-node-c6b89                                 1/1     Running   0             8d
calico-node-dpbkw                                 1/1     Running   0             8d
calico-node-gp9wj                                 1/1     Running   0             8d
coredns-55cb58b774-49chw                          1/1     Running   2 (12d ago)   12d
coredns-55cb58b774-5hr2n                          1/1     Running   1 (12d ago)   12d
etcd-gebo-mza-master                              1/1     Running   1 (18d ago)   18d
kube-apiserver-gebo-mza-master                    1/1     Running   1 (18d ago)   18d
kube-controller-manager-gebo-mza-master           1/1     Running   1 (18d ago)   18d
kube-proxy-2dlzj                                  1/1     Running   2 (12d ago)   18d
kube-proxy-b8swr                                  1/1     Running   1 (12d ago)   18d
kube-proxy-fmpx6                                  1/1     Running   1 (12d ago)   18d
kube-proxy-vgjvp                                  1/1     Running   1 (18d ago)   18d
kube-scheduler-gebo-mza-master                    1/1     Running   1 (18d ago)   18d
node-shell-bdda472f-78ee-4670-be49-b998f5b7b7a4   1/1     Running   0             12m
```

```bash
diegoc@diegoc-ThinkPad-E15-Gen-2:~/Documents/workspace-itc/gebo-mza/Manifests/echo-test$ kubectl get nodes -o wide
NAME                 STATUS   ROLES                       AGE   VERSION   INTERNAL-IP      EXTERNAL-IP   OS-IMAGE             KERNEL-VERSION     CONTAINER-RUNTIME
gebo-mza-master      Ready    control-plane               18d   v1.30.6   192.168.48.236   <none>        Ubuntu 24.04.1 LTS   6.8.0-47-generic   containerd://1.7.22
gebo-mza-worker-01   Ready    control-plane,etcd,worker   18d   v1.30.6   192.168.48.237   <none>        Ubuntu 24.04.1 LTS   6.8.0-47-generic   containerd://1.7.22
gebo-mza-worker-02   Ready    control-plane,etcd,worker   18d   v1.30.6   192.168.48.238   <none>        Ubuntu 24.04.1 LTS   6.8.0-48-generic   containerd://1.7.22
gebo-mza-worker-03   Ready    control-plane,etcd,worker   18d   v1.30.6   192.168.48.241   <none>        Ubuntu 24.04.1 LTS   6.8.0-47-generic   containerd://1.7.22
```

Para obtener detalles del hardware de los nodos del clúster de Kubernetes, puedes usar comandos que recojan información desde cada nodo. Por ejemplo :

El comando kubectl describe node
kubectl describe node <nombre-del-nodo> te muestra información detallada sobre el nodo, incluyendo la capacidad de CPU y memoria:

```bash
diegoc@diegoc-ThinkPad-E15-Gen-2:~/Documents/workspace-itc/gebo-mza/Manifests$ kubectl describe node gebo-mza-master
Name:               gebo-mza-master
Roles:              control-plane
Labels:             beta.kubernetes.io/arch=amd64
                    beta.kubernetes.io/os=linux
                    kubernetes.io/arch=amd64
                    kubernetes.io/hostname=gebo-mza-master
                    kubernetes.io/os=linux
                    node-role.kubernetes.io/control-plane=
                    node.kubernetes.io/exclude-from-external-load-balancers=
Annotations:        kubeadm.alpha.kubernetes.io/cri-socket: unix:///var/run/containerd/containerd.sock
                    node.alpha.kubernetes.io/ttl: 0
                    projectcalico.org/IPv4Address: 192.168.48.236/23
                    projectcalico.org/IPv4IPIPTunnelAddr: 172.16.186.64
                    volumes.kubernetes.io/controller-managed-attach-detach: true
CreationTimestamp:  Fri, 25 Oct 2024 14:20:24 -0300
Taints:             node-role.kubernetes.io/control-plane:NoSchedule
Unschedulable:      false
Lease:
  HolderIdentity:  gebo-mza-master
  AcquireTime:     <unset>
  RenewTime:       Wed, 13 Nov 2024 13:08:45 -0300
Conditions:
  Type                 Status  LastHeartbeatTime                 LastTransitionTime                Reason                       Message
  ----                 ------  -----------------                 ------------------                ------                       -------
  NetworkUnavailable   False   Tue, 05 Nov 2024 10:08:37 -0300   Tue, 05 Nov 2024 10:08:37 -0300   CalicoIsUp                   Calico is running on this node
  MemoryPressure       False   Wed, 13 Nov 2024 13:05:59 -0300   Fri, 25 Oct 2024 14:20:24 -0300   KubeletHasSufficientMemory   kubelet has sufficient memory available
  DiskPressure         False   Wed, 13 Nov 2024 13:05:59 -0300   Fri, 25 Oct 2024 14:20:24 -0300   KubeletHasNoDiskPressure     kubelet has no disk pressure
  PIDPressure          False   Wed, 13 Nov 2024 13:05:59 -0300   Fri, 25 Oct 2024 14:20:24 -0300   KubeletHasSufficientPID      kubelet has sufficient PID available
  Ready                True    Wed, 13 Nov 2024 13:05:59 -0300   Fri, 25 Oct 2024 15:17:03 -0300   KubeletReady                 kubelet is posting ready status
Addresses:
  InternalIP:  192.168.48.236
  Hostname:    gebo-mza-master
Capacity:
  cpu:                8
  ephemeral-storage:  50254368Ki
  hugepages-2Mi:      0
  memory:             16375728Ki
  pods:               110
Allocatable:
  cpu:                8
  ephemeral-storage:  46314425473
  hugepages-2Mi:      0
  memory:             16273328Ki
  pods:               110
System Info:
  Machine ID:                 d2216ecb732141098a0349cac12b9b56
  System UUID:                d2216ecb-7321-4109-8a03-49cac12b9b56
  Boot ID:                    02409953-9532-4020-bea2-3f95a1c4e072
  Kernel Version:             6.8.0-47-generic
  OS Image:                   Ubuntu 24.04.1 LTS
  Operating System:           linux
  Architecture:               amd64
  Container Runtime Version:  containerd://1.7.22
  Kubelet Version:            v1.30.6
  Kube-Proxy Version:         v1.30.6
Non-terminated Pods:          (8 in total)
  Namespace                   Name                                               CPU Requests  CPU Limits  Memory Requests  Memory Limits  Age
  ---------                   ----                                               ------------  ----------  ---------------  -------------  ---
  kube-system                 calico-node-8p9rj                                  250m (3%)     0 (0%)      0 (0%)           0 (0%)         8d
  kube-system                 etcd-gebo-mza-master                               100m (1%)     0 (0%)      100Mi (0%)       0 (0%)         18d
  kube-system                 kube-apiserver-gebo-mza-master                     250m (3%)     0 (0%)      0 (0%)           0 (0%)         18d
  kube-system                 kube-controller-manager-gebo-mza-master            200m (2%)     0 (0%)      0 (0%)           0 (0%)         18d
  kube-system                 kube-proxy-vgjvp                                   0 (0%)        0 (0%)      0 (0%)           0 (0%)         18d
  kube-system                 kube-scheduler-gebo-mza-master                     100m (1%)     0 (0%)      0 (0%)           0 (0%)         18d
  kube-system                 node-shell-bdda472f-78ee-4670-be49-b998f5b7b7a4    0 (0%)        0 (0%)      0 (0%)           0 (0%)         23m
  metallb-system              metallb-speaker-c4hhm                              0 (0%)        0 (0%)      0 (0%)           0 (0%)         18d
Allocated resources:
  (Total limits may be over 100 percent, i.e., overcommitted.)
  Resource           Requests    Limits
  --------           --------    ------
  cpu                900m (11%)  0 (0%)
  memory             100Mi (0%)  0 (0%)
  ephemeral-storage  0 (0%)      0 (0%)
  hugepages-2Mi      0 (0%)      0 (0%)
Events:              <none>
```
